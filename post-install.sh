sudo pacman -S git bspwm sxhkd feh polybar qt6ct qt6ct lxappearance nvim --noconfirm 

git clone aur.archlinux.org/paru-bin
cd paru-bin 
makepkg -si 

paru -S brave-bin 

install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc

xsetroot -cursor_name left_ptr
